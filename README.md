# Seemplicity_project

Read groups and projects from gitlab repo

Python version used is 3.6

Python packages that should be installed:
- python-gitlab version 2.9.0
- sqlite3 (if not installed by default)

The script takes as parameter the gitlab group id and it uses sqlite to store information.

Connection with gitlab repository is configured in gitlab_config.cfg file
