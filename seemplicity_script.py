import sys
import gitlab
from gitlab import GitlabHttpError
import sqlite3
from sqlite3 import Error

def create_db_connection(db_name):
    """ create a database connection to a SQLite database """
    conn = None
    try:
        conn = sqlite3.connect(db_name)
    except Error as e:
        print(e)
    return conn

def create_db_tables(conn):
    cur = conn.cursor()
    cur.execute('''PRAGMA foreign_keys=ON;''')
    cur.execute('''CREATE TABLE IF NOT EXISTS Gitlab_Project
             (Id INT PRIMARY KEY  NOT NULL,
             Name CHAR(100) NOT NULL);''')

    cur.execute('''CREATE TABLE IF NOT EXISTS Gitlab_User
                 (Id INT PRIMARY KEY  NOT NULL,
                 UserName CHAR(100) NOT NULL,
                 Name CHAR(100) NOT NULL);''')

    cur.execute('''CREATE TABLE IF NOT EXISTS Gitlab_Project_User
                     (Project_id INT NOT NULL,
                     User_id INT NOT NULL,
                     FOREIGN KEY(Project_id) REFERENCES Gitlab_Project(Id),
                     FOREIGN KEY(User_id) REFERENCES Gitlab_User(Id),
                     PRIMARY KEY(Project_id, User_id));''')

def fill_users_table(users_list, conn):
    with conn:
        cur = conn.cursor()
        users_to_insert = [(user.id, user.username, user.name) for user in users_list]
        cur.executemany('INSERT OR IGNORE INTO Gitlab_User(Id, UserName, Name) VALUES(?,?,?)', users_to_insert)

def fill_projects_table(projects_list, conn):
    with conn:
        cur = conn.cursor()
        projects_to_insert = [(project.id, project.name) for project in projects_list]
        cur.executemany('INSERT OR IGNORE INTO Gitlab_Project(Id, Name) VALUES(?,?)', projects_to_insert)

def fill_project_user_table(user_id, project_list, conn):
    with conn:
        cur = conn.cursor()
        data_to_insert = [(project.id, user_id) for project in project_list]
        cur.executemany('INSERT OR IGNORE INTO Gitlab_Project_User(Project_id, User_id) VALUES(?,?)', data_to_insert)

def get_gitlab_data(group_id, conn):

    try:
        gl_connect = gitlab.Gitlab.from_config('gitlab', ['gitlab_config.cfg'])
    except GitlabHttpError as e:
        print(e)
    group_details = gl_connect.groups.get(group_id)
    projects = group_details.projects.list()
    if projects:
        fill_projects_table(projects, conn)
    members = group_details.members.list()
    if members:
        fill_users_table(members, conn)
        for member in members:
            user = gl_connect.users.get(member.id)
            project_users = [project for project in user.projects.list() if project.namespace["id"] == group_id]
            if project_users:
                fill_project_user_table(user.id, project_users, conn)

def main():
    no_arguments = len(sys.argv) - 1
    if no_arguments == 1:
        conn = create_db_connection(r"gitlab.db")
        with conn:
            try:
                create_db_tables(conn)
                get_gitlab_data(sys.argv[1], conn)
                conn.commit()
                print('Success!')
            except conn.Error as e:
                conn.rollback()
                print(e)

if __name__ == '__main__':
    main()